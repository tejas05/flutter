//Program 4
mixin Test {
  int x = 20;
  void demo() {
    print("In Test Demo");
  }

  void fun2();
}

class Test2 with Test {
  void demo() {
    print("In Test2 Demo");
    super.demo();
  }

  void fun2() {
    print("In Fun 2");
  }
}

void main() {
  Test2 obj = new Test2();
  obj.demo();
}
