//Program 6
class Demo2 {
  Demo2() {
    print("In Demo2");
  }
}

mixin Demo on Demo2 {
  void fun() {
    print("In fun");
  }
}

class Test extends Demo2 with Demo {
  Test() {
    print("In Test");
  }
}

void main() {
  Test obj = new Test();
}
