//Program 5
mixin Test {
  void demo() {
    print("In Test Demo");
  }
}
mixin Test2 {
  void demo() {
    print("In Test2 Demo");
  }
}

class Test3 with Test, Test2 {
  void demo() {
    print("In Test3 Demo");
    super.demo();
  }

  void fun2() {
    print("In Fun2");
  }
}

void main() {
  Test3 obj = new Test3();
  obj.demo();
}
