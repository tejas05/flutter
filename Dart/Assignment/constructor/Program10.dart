//Program 10 

class Demo {
  Demo() {
    print("In Demo");
  }
  factory Demo() {
    print("In Demo Factory");
    return Test_private();
  }
}

void main() {
  Test obj = new Test();
}


//Error
//Return Type is Private and class is not a private.
// here the class Test is not a type of private.
