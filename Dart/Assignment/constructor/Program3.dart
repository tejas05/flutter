//Program 3
class Demo {
  final int? x;
  final String? str;

  const Demo(this.x, this.str);
}

void main() {
  Demo obj1 = const Demo(10, "Core2web");
  print(obj1.hashCode);
  Demo obj2 = const Demo(10, "Biencaps");
  print(obj2.hashCode);
}

//Output:
//62708299
//120083679
// In a const constructor only one object is created
// but if we pass the different data then the hashcode is different 
// if we pass the same data as obj1 then the output is same hashcode