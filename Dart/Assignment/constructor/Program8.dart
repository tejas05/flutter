class Player {
  int? jerNo;
  String? pName;
  const Player(this.jerNo, this.pName);
}

void main() {
  Player obj = const (45, "Rohit");
}


//Error

// All instant variable must be initialized as final in const constructor.
// when we create the object of const constructor then call
// the constructor by class name like const Player.
