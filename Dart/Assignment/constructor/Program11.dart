//Program 11
class Test {
  Test_private() {
    print("In Demo");
  }

  factory Test() {
    print("In Demo Factory");
    return Test_Private();
  }
}

void main() {
  Test obj = new Test();
}

//Error
// we return the Private method 
// but the private method not found 