//Program 1

class Test {
  int x = 10;
  Test(this.x);
}

class Test2 extends Test {
  Test2(super.x);
}

void main() {
  Test2 obj = Test2(10);
  Test obj2 = Test(30);
  obj.x = 19;

  print(obj.x);
  print(obj2.x);
}


//Output:
//19
//30
//In this the Parent class assings the value 30
//In child class extends Parent class inherits the variable 
