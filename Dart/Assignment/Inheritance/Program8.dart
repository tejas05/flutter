//Program 8

class Parent {
  Parent() {
    print("In Parent Constructor");
  }
}

class Child extends Parent {
  Child() {
    super();
    print("In Child Constructor");
  }
}

void main() {
  Child obj = new Child();
}


//Error
// Call Method should be written in parent class 
//if we are calling the constructor from Child class