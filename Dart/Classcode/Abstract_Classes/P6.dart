abstract class Developer{
void develop(){
print("we build Software");
}
void devType();
}
class MobileDev extends Developer{
void devType(){
print("Flutter Dev");
}
}
class WebDev extends Developer{
void devType(){
print("Web Dev");
}
}
void main(){
Developer obj1=new MobileDev();
obj1.develop();
obj1.devType();

Developer obj2=new WebDev();
obj2.develop();
obj2.devType();

WebDev obj3=new WebDev();
obj3.develop();
obj3.devType();
}

