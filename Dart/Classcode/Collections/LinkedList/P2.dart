import 'dart:collection';

final class Company extends LinkedListEntry<Company>{
int empCount;
String compName;
double rev;

Company (this.empCount,this.compName,this.rev);

@override
String toString(){
return "EmpCount=$empCount,CompName=$compName,Revenue =$rev";
}
}
void main(){

var cmpObj=LinkedList<Company>();
cmpObj.add(Company(700,"Veritas",10000.0));
cmpObj.add(Company(1000,"Cummins",15000.0));
cmpObj.add(Company(1000,"VMware",2000.0));

print(cmpObj);
}

