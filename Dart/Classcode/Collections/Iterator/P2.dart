void main(){

var players=["Rohit","Shubman","Virat","KlRahul","Shreyas","Hardik"];

//any()

var retVal1=players.any((player)=>player[0] == "Z");
var retVal2=players.any((player)=>player.length <=5);
var retVal3=players.any((player)=>player[0] =="V");
print(retVal1);
print(retVal2);
print(retVal3);



//contains()

var retVal4=players.contains("Shreyas");
print(retVal4);



//elementAt()

var retVal5=players.elementAt(5);
print(retVal5);



//every()

var retVal6=players.every((player)=>player[0] =="Z");
var retVal7=players.every((player)=>player.length >4);

print(retVal6);
print(retVal7);



//firstWhere()

//var retVal8=players.firstWhere((player) => player == "S");
//print(retVal8);


//lastWhere()

//var retVal9=players.lastWhere((player) => player == "S");
//print(retVal9);



// fold()
var initVal="";
var retVal10=players.fold(initVal,(prevVal,player) => prevVal + player);
print(retVal10);



//followdBy()

var newPlayer =["Jadeja","Bumrah"];
var retVal11 = players.followedBy(newPlayer);
print(retVal11);




//forEach()
players.forEach((element) => 
print(element));




//join()
var retVal12=players.join("->");
print(retVal12);



//map()

var retVal13 = players.map((player)=>player+"Ind");
print(retVal13);



//reduce()

var retVal14=players.reduce((value,player) => value + player);
print(retVal14);



//singleWhere();

var retVal15=players.singleWhere((player) => player.length == 6);
print(retVal15);




//skip()

var retVal16 = players.skip(4);
print(retVal16);



//skipWhile()

var retVal17=players.skipWhile((element)=>element.length == 5);
print(retVal17);




//take()
var retVal18=players.take(5);
print(retVal18);


//takeWhile()

var retVal19=players.takeWhile((element)=>element[0] == "R");
print(retVal19);




//toList()

var retVal20=players.toList();
print(retVal20);




//toSet()

var retVal21=players.toSet();
print(retVal21);




//where()

var retVal22 = players.where((player)=>player[0] == "S");
print(retVal22);



















}

