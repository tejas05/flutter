import 'dart:collection';

void main(){
var player=SplayTreeMap();

player[18]="Virat";
print(player);

player..addAll({45:"Rohit"});
print(player);

player.addEntries({7:"MSD",1:"KLRahul"}.entries);
print(player);

player.update(18,(value) =>"Virat Kohli");
print(player);

var constPlayer=UnmodifiableMapBase(player);
print(constPlayer);

}

