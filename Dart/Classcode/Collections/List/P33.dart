void main(){
var progLang=List.empty(growable:true);

progLang.add("Dart");
progLang.add("Java");
progLang.add("Python");
progLang.add("CPP");

print(progLang);

var lang=["ReactJS","Springboot","Flutter"];

progLang.addAll(lang);

print(progLang);

progLang.insert(3,"Dart");
print(progLang);

progLang.insertAll(3,["GO","Swift"]);
print(progLang);

progLang.replaceRange(3,7,{"C","Ruby"});
print(progLang);

progLang.remove("ReactJS");
print(progLang);

progLang.add("Dart");
print(progLang);

progLang.remove("Dart");
print(progLang);

progLang.removeAt(5);
print(progLang);
}









