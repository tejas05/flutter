///// Multilevel Inheritance /////

class ICC{
ICC(){
print("In Parent constructor");
}
}
class BCCI extends ICC{
BCCI(){
print("BCCI Constructor");
}
}

class IPL extends BCCI{
IPL(){
print("IPL constructor");
}
}
void main(){
IPL obj =new IPL();
}

