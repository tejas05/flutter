////Optional Parameter with default values////////


class Company {
int? empCount;
String? compName;

Company(this.empCount,{this.compName="Biencaps"});

void compInfo(){
Company obj1=new Company (100);
Company obj2= new Company(200,"pubmatic");
obj1.compInfo();
obj2.compInfo();
}

