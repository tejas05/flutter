///////Optional Named Parameter/////////



class Company{
int? empCount;
String?compName;

Company({this.empCount,this.compName="Biencaps"});

void compInfo(){

print(empCount);
print(compName);
}
}
void main(){
Company obj1=new Company(empCount:100);
Company obj2=new Company(empCount:200,compName:"Pubmatic");
obj1.compInfo();
obj2.compInfo();
}
