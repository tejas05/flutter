import 'dart:io';
class Employee{
String? empName="Tejas";
int? empId=120;
double? empSalary=1.2;

void empInfo(){
print("Employee name=$empName");
print("Employee id=$empId");
print("Employee salary=$empSalary");
}
}

void main(){
Employee empObj=new Employee();
empObj.empInfo();

print("");
print("Enter employee Id");
int? empId=int.parse(stdin.readLineSync()!);

print("Enter employee name");
String? empName=stdin.readLineSync();

print("Enter employee salary");
double? empSalary=double.parse(stdin.readLineSync()!);

empObj.empInfo();

}
