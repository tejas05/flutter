//Type 1//

class Demo{

int? _x;
String? _str;
double? _sal;

Demo(this._x,this._str,this._sal);

int? getX(){
return _x;
}
double? getSal(){
return _sal;
}
String? getStr(){
return _str;
}
}
