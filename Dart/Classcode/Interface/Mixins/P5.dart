abstract class Demo1{
void fun1(){
print("In fun1-Demo1");
}
void fun2();
}
abstract class Demo2{
void fun3(){
print("In Demo2-Fun3");
}
void fun4();
}
class DemoChild implements Demo1,Demo2{
void fun2(){
print("In Demo Child - fun2 ");
}
void fun4(){
print("In Demo child - fun4");
}
}
void main(){
DemoChild obj = new DemoChild();
obj.fun2();
obj.fun4();
}

